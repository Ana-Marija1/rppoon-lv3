﻿using System;

namespace Rppoon_LV3_Z4
{
    class Program
    {
        static void Main(string[] args)
        {

            String author = "Ana-Marija Katic";
            String title = "Zadatak 4.";
            String text = "Doslo je do greske!";
            DateTime time = DateTime.Now;
            Category level = Category.ERROR;
            ConsoleColor color = ConsoleColor.Red;
            ConsoleNotification consoleNotification = new ConsoleNotification(author, title, text, time, level, color);
            NotificationManager notificationManager = new NotificationManager();
            notificationManager.Display(consoleNotification);

            author = "Ana-Marija Katic";
            title = "Zadatak 4.";
            text = "Upozorenje!";
            time = DateTime.Now;
            level = Category.ALERT;
            color = ConsoleColor.Yellow;
            consoleNotification = new ConsoleNotification(author, title, text, time, level, color);
            notificationManager.Display(consoleNotification);

            author = "Ana-Marija Katic";
            title = "Zadatak 4.";
            text = "Napisati program za testiranje funkcionalnosti primjera 3. Iskoristiti NotificationManager i ConsoleNotification klase, odnosno njihove objekte kako bi se prikazala obavijest na konzoli.";
            time = DateTime.Now;
            level = Category.INFO;
            color = ConsoleColor.Blue;
            consoleNotification = new ConsoleNotification(author, title, text, time, level, color);
            notificationManager.Display(consoleNotification);

        }
    }
}
