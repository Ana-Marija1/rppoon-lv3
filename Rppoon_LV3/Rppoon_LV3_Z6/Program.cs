﻿using System;

namespace Rppoon_LV3_Z6
{
    class Program
    {
        //Zadatak 6.
        //Napisati konkretnu klasu za direktora koja izlaže metode za stvaranje podrazumijevanih INFO, ALERT i
        //ERROR razina obavijesti(koristiti enum Category). Jedini parametar koji se predaje metodi je autor, sve
        //ostale parametre postavlja se na podrazumijevane vrijednosti prema Vašem izboru.
        static void Main(string[] args)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            Director director = new Director();

            director.ErrorNotification(notificationBuilder, "Ana-Marija Katic");
            ConsoleNotification errorNotification = notificationBuilder.Build();
            Display(errorNotification);

            director.AlertNotification(notificationBuilder, "Ana-Marija Katic");
            ConsoleNotification alertNotification = notificationBuilder.Build();
            alertNotification = notificationBuilder.Build();
            Display(alertNotification);

            director.InfoNotification(notificationBuilder, "Ana-Marija Katic");
            ConsoleNotification infoNotification = notificationBuilder.Build();
            infoNotification = notificationBuilder.Build();
            Display(infoNotification);

        }
        static void Display(ConsoleNotification notification)
        {
            Console.ForegroundColor = notification.Color;
            Console.Write(notification.Author + ": ");
            Console.WriteLine(notification.Title);
            Console.WriteLine(notification.Timestamp.ToString());
            Console.WriteLine(notification.Text);
            Console.WriteLine(notification.Level);
            Console.ResetColor();
        }
    }
}
