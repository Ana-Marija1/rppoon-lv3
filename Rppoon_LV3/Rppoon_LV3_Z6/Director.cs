﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV3_Z6
{
    class Director
    {
        public void ErrorNotification(IBuilder builder,String author)
        {
            builder.SetAuthor(author);
            builder.SetTitle("Obavijest greske!");
            builder.SetTime(DateTime.Now);
            builder.SetLevel(Category.ERROR);
            builder.SetColor(ConsoleColor.Red);
        }

        public void AlertNotification(IBuilder builder, String author)
        {
            builder.SetAuthor(author);
            builder.SetTitle("Obavijest upozorenja!");
            builder.SetTime(DateTime.Now);
            builder.SetLevel(Category.ALERT);
            builder.SetColor(ConsoleColor.Yellow);
        }

        public void InfoNotification(IBuilder builder, String author)
        {
            builder.SetAuthor(author);
            builder.SetTitle("Obavijest s informacijama!");
            builder.SetTime(DateTime.Now);
            builder.SetLevel(Category.INFO);
            builder.SetColor(ConsoleColor.Blue);
        }


    }
}
