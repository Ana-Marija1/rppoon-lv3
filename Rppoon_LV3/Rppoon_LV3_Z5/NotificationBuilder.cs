﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV3_Z5
{
    class NotificationBuilder:IBuilder
    {
        private String author = "Author";
        private String title;
        private String text;
        private DateTime time=DateTime.Now;
        private Category level=Category.INFO;
        private ConsoleColor color = ConsoleColor.Gray;
        
        public ConsoleNotification Build()
        {
            return new ConsoleNotification(author, title, text, time, level, color);
        }
        public IBuilder SetAuthor(string author)
        {
            this.author = author;
            return this;
        }
        public IBuilder SetTitle(String title)
        {
            this.title = title;
            return this;
        }
        public IBuilder SetText(String text)
        {
            this.text = text;
            return this;
        }
        public IBuilder SetTime(DateTime time)
        {
            this.time = time;
            return this;
        }
        public IBuilder SetLevel(Category level)
        {
            this.level = level;
            return this;
        }
        public IBuilder SetColor(ConsoleColor color)
        {
            this.color = color;
            return this;
        }
        
    }
}
