﻿using System;

namespace Rppoon_LV3_Z5
{ 
    class Program
    {
        static void Main(string[] args)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetAuthor("Ana-Marija Katic");
            notificationBuilder.SetText("Zadatak 5.");
            notificationBuilder.SetLevel(Category.ALERT);
            notificationBuilder.SetColor(ConsoleColor.Magenta);
            ConsoleNotification consoleNotification = notificationBuilder.Build();
            Display(consoleNotification);
        }

        static void Display(ConsoleNotification notification)
        {
            Console.ForegroundColor = notification.Color;
            Console.Write(notification.Author + ": ");
            Console.WriteLine(notification.Title);
            Console.WriteLine(notification.Timestamp.ToString());
            Console.WriteLine(notification.Text);
            Console.WriteLine(notification.Level);
            Console.ResetColor();
        }

    }
}
