﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV3
{
    class Dataset:Prototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(Dataset dataset)
        {
            data = new List<List<string>>();
            foreach (List<string> information in dataset.data)
            {
                List<string> info = new List<string>();
                foreach (string variable in information)
                {
                    info.Add(variable);
                }
                data.Add(info);
            }
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }
        public Prototype Clone()
        {
            return new Dataset(this);
        }


    }
}
