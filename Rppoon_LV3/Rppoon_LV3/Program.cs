﻿using System;
using System.Collections.Generic;

namespace Rppoon_LV3
{
    class Program
    {
        //Zadatak 1.
        //Izmijeniti klasu iz primjera 1.2 tako da umjesto plitkog vrši duboko(engl.deep) kopiranje.Za potrebe
        //testiranja napraviti malenu CSV datoteku po uzoru na primjer u nastavku.Je li potrebno duboko kopiranje za
        //klasu u pitanju?

        //Nije potrebno duboko kopiranje za klasu u pitanju jer klasa nema privatne članove kojima se pristupa izvana.
        //Problem bi mogao nastati brisanjem originalnog objekta klase jer se kod plitkog kopiranja kopiraju samo reference
        //,a ne i objekti na koje se odnose.

        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("D:\\Ana-Marija\\Faks\\IV. semestar\\RPPOON\\Rppoon_LV3\\Rppoon_LV3\\Rppoon_LV3\\CSVfile.txt");
            Print(dataset.GetData());
            Dataset prototype = (Dataset)dataset.Clone();
            Print(prototype.GetData());

        }
        static void Print(IList<List<string>> data)
        {
            foreach (List<string> information in data)
            {
                foreach (string variable in information)
                {
                    Console.WriteLine(variable);
                }
            }
        }
    }
}
