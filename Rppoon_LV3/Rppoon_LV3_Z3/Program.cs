﻿using System;

namespace Rppoon_LV3_Z3
{
    class Program
    {
        //Zadatak 3.
        //Kreirajte singleton koji predstavlja logger.Navedeni logger treba omogućiti logiranje u zadanu datoteku koju
        //se postavlja preko odgovarajućeg svojstva, a u konstruktoru se postavlja na neku podrazumijevanu
        //vrijednost.Ako je datoteka postavljena na jednom mjestu u tekstu programa, hoće li uporaba loggera na
        //drugim mjesta u testu programa pisati u istu datoteku (pretpostavka je kako nisu ponovo postavljene)?
        
        //Ako je datoteka postavljena na jednom mjestu u tekstu programa uporabom loggera na drugom mjestu u tekst
        // programa pisat će se u istu datoteku jer je putanja do datoteke jednaka. Ako se to želi izbjeći treba se
        // postaviti nova putanja do druge datoteke.

        static void Main(string[] args)
        {
            Logger logger = Logger.GetInstance();
            Console.WriteLine("Enter the file title: ");
            string fileTitle = Console.ReadLine();
            logger.Log(fileTitle);
            Console.WriteLine("Enter a line of text: ");
            string textLine = Console.ReadLine();
            logger.Log(textLine);
            logger.FilePath ="D:\\Ana-Marija\\Faks\\IV. semestar\\RPPOON\\Rppoon_LV3\\Rppoon_LV3\\Rppoon_LV3_Z3\\Logger_Z3.txt";
            Console.WriteLine("Enter another line of text: ");
            string nextTextLine = Console.ReadLine();
            logger.Log(nextTextLine);
        }
    }
}
