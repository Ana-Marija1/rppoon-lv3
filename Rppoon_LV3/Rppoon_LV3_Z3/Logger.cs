﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV3_Z3
{
    class Logger
    {
        private static Logger instance;
        public string FilePath { private get; set; }
        
        private Logger()
        {
            this.FilePath = "D:\\Ana-Marija\\Faks\\IV. semestar\\RPPOON\\Rppoon_LV3\\Rppoon_LV3\\Rppoon_LV3_Z3\\Logger_Z3.txt";
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void Log(string message)
        {
            using (System.IO.StreamWriter writer= new System.IO.StreamWriter(this.FilePath,true))
            {
                writer.WriteLine(message);
            }
        }
    }
}
