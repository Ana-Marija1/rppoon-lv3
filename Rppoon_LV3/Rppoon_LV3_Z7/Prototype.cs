﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV3_Z7
{
    public interface Prototype
    {
        Prototype Clone();
    }
}
