﻿using System;

namespace Rppoon_LV3_Z7
{
    class Program
    { 
        //Ima li u konkretnom slučaju razlike između plitkog i dubokog kopiranja?

        //Nema razlike između plitkog i dubokog kopiranja pa nema potrebe za dubokim
        //kopiranjem jer svojstva klase imaju privatne settere i ne mogu se mijenjati.
        static void Main(string[] args)
        {

            String author = "Ana-Marija Katic";
            String title = "Zadatak 7.";
            String text = "Klasu ConsoleNotification iz primjera 3 izmijeniti tako da ugrađuje sučelje Protoype iz primjera 1.2. Ima li u konkretnom slučaju razlike između plitkog i dubokog kopiranja?";
            DateTime time = DateTime.Now;
            Category level = Category.INFO;
            ConsoleColor color = ConsoleColor.Blue;
            ConsoleNotification consoleNotification = new ConsoleNotification(author, title, text, time, level, color);
            Display(consoleNotification);
            ConsoleNotification prototype = (ConsoleNotification)consoleNotification.Clone();
            Display(prototype);


        }

        static void Display(ConsoleNotification notification)
        {
            Console.ForegroundColor = notification.Color;
            Console.Write(notification.Author + ": ");
            Console.WriteLine(notification.Title);
            Console.WriteLine(notification.Timestamp.ToString());
            Console.WriteLine(notification.Text);
            Console.WriteLine(notification.Level);
            Console.ResetColor();
        }
    }
}
