﻿using System;

namespace Rppoon_LV3_Z2
{
    class Program
    {
        //Zadatak 2.
        //Kreirajte singleton koji predstavlja generator matrica.Klasa treba pružiti metodu kojoj se predaju dimenzije
        //matrice (broj redaka i broj stupaca) i koja vraća matricu popunjenu realnim pseudo-slučajnim brojevima iz
        //intervala[0, 1). Koliko odgovornosti ima navedena metoda ?

        //Navedena metoda ima dvije odgovornosti, definira potrebno ponašanje/funkcionalnost,tj. stvara matricu i brine
        // se o svom instanciranju i održavanju jedne instance.
        static void Main(string[] args)
        {
            MatrixGenerator matrixGenerator = MatrixGenerator.GetInstance();
            Console.WriteLine("Enter the lenght of the outer array:");
            int rows=Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the lenght of the inner array:");
            int columns= Convert.ToInt32(Console.ReadLine());
            Print(matrixGenerator.DoubleMatrix(rows, columns));
           

        }
        static void Print(double[][] matrix)
        {
           for(int i = 0; i < matrix.Length; i++)
            {
                for(int j = 0; j < matrix[i].Length; j++)
                {
                    Console.Write(matrix[i][j]+"\t");
                }
                Console.WriteLine();
            }
        }

    }
}
